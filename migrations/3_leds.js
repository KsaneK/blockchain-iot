const Led = artifacts.require("Led");
const BetterLed = artifacts.require("BetterLed");

module.exports = function(deployer) {
  deployer.deploy(Led);
  deployer.deploy(BetterLed);
};
