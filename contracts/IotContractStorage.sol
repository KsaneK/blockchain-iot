pragma experimental ABIEncoderV2;

interface IIotContract {
  function name() external view returns (string memory);
  function getOwner() external view returns (address);
  function getComponent() external view returns (bytes32[] memory);
}

contract IotContractStorage {

  address private owner;

  struct IotContract {
    string name;
    address addr;
    bool is_active;
  }

  uint32 contractsLen = 0;
  uint32 activeContracts = 0;
  mapping(uint32 => address) contractAddresses;
  mapping(address => IotContract) iotContracts;
  mapping(address => mapping(address => bool)) favourites;

  event ContractAdded(IotContract iotContract);
  event ContractDeactivated(IotContract iotContract);
  event ContractActivated(IotContract iotContract);


  constructor() public {
    owner = msg.sender;
  }

  modifier isOwner() {
    require(msg.sender == owner, "Caller is not owner");
    _;
  }

  function changeOwner(address newOwner) public isOwner {
    owner = newOwner;
  }

  function getOwner() external view returns (address) {
    return owner;
  }

  function addContract(address addr) public returns (string memory) {
    require(iotContracts[addr].addr == address(0x0), "Contract with this address already exists");
    IIotContract contractToAdd = IIotContract(addr);
    contractAddresses[contractsLen] = addr;
    string memory contractName = contractToAdd.name();
    iotContracts[addr] = IotContract(contractName, addr, true);
    contractsLen += 1;
    activeContracts += 1;
    emit ContractAdded(iotContracts[addr]);
    return contractName;
  }

  function deactivateContract(address addr) public {
    require(iotContracts[addr].addr != address(0x0), "Contract doesn't exist in storage");
    require(iotContracts[addr].is_active == true, "Contract is already inactive");
    IIotContract contractToDeactivate = IIotContract(addr);
    require(contractToDeactivate.getOwner() == msg.sender, "You're not owner of this contract");
    iotContracts[addr].is_active = false;
    activeContracts -= 1;
    emit ContractDeactivated(iotContracts[addr]);
  }

  function activateContract(address addr) public {
    require(iotContracts[addr].addr != address(0x0), "Contract doesn't exist in storage");
    require(iotContracts[addr].is_active == false, "Contract is already active");
    IIotContract contractToDeactivate = IIotContract(addr);
    require(contractToDeactivate.getOwner() == msg.sender, "You're not owner of this contract");
    iotContracts[addr].is_active = true;
    activeContracts += 1;
    emit ContractActivated(iotContracts[addr]);
  }

  function isOwnerOf(address addr) public view returns (bool) {
    IIotContract contractToDeactivate = IIotContract(addr);
    return contractToDeactivate.getOwner() == msg.sender;
  }

  function getContractCount() public view returns (uint32) {
    return activeContracts;
  }

  function getContracts() public view returns (IotContract[] memory) {
    IotContract[] memory ret = new IotContract[](activeContracts);
    uint32 ind = 0;
    for (uint32 i = 0; i < contractsLen; i++) {
      if (iotContracts[contractAddresses[i]].is_active) {
        ret[ind] = iotContracts[contractAddresses[i]];
        ind++;
      }
    }
    return ret;
  }

  function doesExist(address addr) public view returns (bool) {
    return iotContracts[addr].addr != address(0x0);
  }

  function addToFavourite(address addr) public {
    favourites[msg.sender][addr] = true;
  }

  function removeFromFavourite(address addr) public {
    favourites[msg.sender][addr] = false;
  }

  function isFavourite(address addr) public view returns (bool) {
    return favourites[msg.sender][addr];
  }
}
