pragma solidity >=0.4.25 <0.7.0;

contract BetterLed {
  bool state;
  uint8 brightness;
  bytes32[] componentZip;
  address owner;

  event ChangeState(bool _value);
  event ChangeBrightness(uint8 value);

  constructor() public {
    state = false;
    brightness = 100;
    owner = msg.sender;
  }

  function getOwner() public view returns (address) {
    return owner;
  }

  function getComponent() public view returns (bytes32[] memory) {
    return componentZip;
  }

  function setComponent(bytes32[] memory zipFile) public {
    componentZip = zipFile;
  }

  function name() public pure returns(string memory) {
    return "Better LED";
  }

  function getState() public view returns(bool) {
    return state;
  }

  function on() public returns(bool) {
    state = true;
    emit ChangeState(state);
    return true;
  }

  function off() public returns(bool) {
    state = false;
    emit ChangeState(state);
    return true;
  }

  function setBrightness(uint8 newBrightness) public {
    brightness = newBrightness;
    emit ChangeBrightness(brightness);
  }

  function getBrightness() public view returns(uint8) {
    return brightness;
  }
}
