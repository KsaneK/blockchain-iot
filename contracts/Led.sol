pragma solidity >=0.4.25 <0.7.0;

contract Led {
  bool state;
  bytes32[] componentZip;
  address owner;

  event ChangeState(bool _value);

  constructor() public {
    state = false;
    owner = msg.sender;
  }

  function getOwner() public view returns (address) {
    return owner;
  }

  function name() public pure returns (string memory) {
    return "Led";
  }

  function getComponent() public view returns (bytes32[] memory) {
    return componentZip;
  }

  function setComponent(bytes32[] memory zipFile) public {
    componentZip = zipFile;
  }

  function getState() public view returns (bool) {
    return state;
  }

  function on() public {
    state = true;
    emit ChangeState(state);
  }

  function off() public {
    state = false;
    emit ChangeState(state);
  }
}
