pragma solidity >=0.4.25 <0.7.0;

contract Thermometer {
  uint32 temperature;
  bytes32[] componentZip;
  address owner;

  event ChangeTemperature(uint32 value);

  constructor() public {
    temperature = 0;
    owner = msg.sender;
  }

  function getOwner() public view returns (address) {
    return owner;
  }

  function name() public pure returns (string memory) {
    return "Thermometer";
  }

  function getComponent() public view returns (bytes32[] memory) {
    return componentZip;
  }

  function setComponent(bytes32[] memory zipFile) public {
    componentZip = zipFile;
  }

  function getTemperature() public view returns (uint32) {
    return temperature;
  }

  function setTemperature(uint32 temp) public {
    require(msg.sender == owner, "Only owner can set temperature.");
    temperature = temp;
    emit ChangeTemperature(temperature);
  }

}
