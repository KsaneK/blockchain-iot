pragma solidity >=0.4.25 <0.7.0;

contract FuelPump {
  uint fuelStock;
  bytes32[] componentZip;
  address owner;
  uint weiForLiter;
  uint32 notifyThreshold;

  event BoughtFuel(uint32 pumpId, uint liters);
  event SmallReserves(uint fuelStock);

  constructor() public {
    fuelStock = 0;
    weiForLiter = 4500000000000000;
    notifyThreshold = 0;
    owner = msg.sender;
  }

  function getOwner() public view returns (address) {
    return owner;
  }

  function name() public pure returns (string memory) {
    return "Fuel Pump";
  }

  function getComponent() public view returns (bytes32[] memory) {
    return componentZip;
  }

  function setComponent(bytes32[] memory zipFile) public {
    componentZip = zipFile;
  }

  function getStock() public view returns (uint) {
    return fuelStock;
  }

  function getPrice() public view returns (uint) {
    return weiForLiter;
  }

  function addToStock(uint32 stock) public {
    require(msg.sender == owner, "Only owner can add to stock.");
    fuelStock += stock;
  }

  function setNotifyThreshold(uint32 threshold) public {
    require(msg.sender == owner, "Only owner can change notify threshold.");
    notifyThreshold = threshold;
  }

  function changePrice(uint weiPrice) public {
    require(msg.sender == owner, "Only owner can change price.");
    weiForLiter = weiPrice;
  }

  function buyFuel(uint32 pumpId) public payable {
    uint liters = msg.value / weiForLiter;
    fuelStock -= liters;
    emit BoughtFuel(pumpId, liters);
    if (fuelStock < notifyThreshold) {
      emit SmallReserves(fuelStock);
    }
  }
}
