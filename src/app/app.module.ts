import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { RouterModule, Routes } from '@angular/router';
import { ContractListComponent } from './home/contract-list/contract-list.component';
import { ContractAddFormComponent } from './home/contract-add-form/contract-add-form.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContractModule } from './contract/contract.module';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'contracts', component: ContractListComponent},
  {path: 'add', component: ContractAddFormComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ContractListComponent,
    ContractAddFormComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    ContractModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
