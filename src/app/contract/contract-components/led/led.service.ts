import { Injectable } from '@angular/core';
import { Web3Service } from '../../../services/web3.service';
import { BehaviorSubject } from 'rxjs';
import { BaseService } from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class LedService extends BaseService {
  static address = '0xc2331A067871C7939beF877E9541669AdB8e9891';
  static abi = [
    {
      inputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'constructor'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'bool',
          name: '_value',
          type: 'bool'
        }
      ],
      name: 'ChangeState',
      type: 'event'
    },
    {
      constant: true,
      inputs: [],
      name: 'name',
      outputs: [
        {
          internalType: 'string',
          name: '',
          type: 'string'
        }
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getState',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [],
      name: 'on',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [],
      name: 'off',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    }
  ];
  private state: BehaviorSubject<boolean>;

  constructor(web3Service: Web3Service) {
    super(web3Service, LedService.address, LedService.abi);
    this.state = new BehaviorSubject<boolean>(false);
    this.getState();
    this.contract.events.ChangeState({}, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        this.state.next(data.returnValues[0]);
      }
    });
  }

  public on() {
    this.contract.methods.on().send({from: this.web3Service.getAddress()});
  }

  public off() {
    this.contract.methods.off().send({from: this.web3Service.getAddress()});
  }

  public getState() {
    return this.contract.methods.getState().call().then(state => {
      this.state.next(state);
    });
  }

  public getObservableState() {
    return this.state.asObservable();
  }
}
