import { Component, NgZone, OnInit } from '@angular/core';
import { LedService } from './led.service';

@Component({
  selector: 'app-led',
  templateUrl: './led.component.html',
  styleUrls: ['./led.component.css']
})
export class LedComponent implements OnInit {
  name: string;
  addr: string;
  state: boolean;
  stateObservable;

  constructor(private ledService: LedService,
              private ngZone: NgZone) { }

  async ngOnInit() {
    this.name = await this.ledService.getName();
    this.addr = this.ledService.getAddress();
    this.stateObservable = this.ledService.getObservableState();
    this.stateObservable.subscribe(state => {
      this.ngZone.run(() => this.state = state);
    });
  }

  turnOn() {
    this.ledService.on();
  }

  turnOff() {
    this.ledService.off();
  }
}
