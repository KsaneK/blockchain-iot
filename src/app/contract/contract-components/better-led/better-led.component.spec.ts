import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetterLedComponent } from './better-led.component';

describe('BetterLedComponent', () => {
  let component: BetterLedComponent;
  let fixture: ComponentFixture<BetterLedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetterLedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetterLedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
