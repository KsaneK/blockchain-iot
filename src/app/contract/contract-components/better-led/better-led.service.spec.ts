import { TestBed } from '@angular/core/testing';

import { BetterLedService } from './better-led.service';

describe('BetterLedService', () => {
  let service: BetterLedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BetterLedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
