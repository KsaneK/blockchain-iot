import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Web3Service } from '../../../services/web3.service';
import { BaseService } from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class BetterLedService extends BaseService {
  static address = '0xDfcAF6F7B74c38928B36C8d0F2141e29B169B63c';
  static abi = [
    {
      inputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'constructor'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'uint8',
          name: 'value',
          type: 'uint8'
        }
      ],
      name: 'ChangeBrightness',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'bool',
          name: '_value',
          type: 'bool'
        }
      ],
      name: 'ChangeState',
      type: 'event'
    },
    {
      constant: true,
      inputs: [],
      name: 'name',
      outputs: [
        {
          internalType: 'string',
          name: '',
          type: 'string'
        }
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getState',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [],
      name: 'on',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [],
      name: 'off',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'uint8',
          name: 'newBrightness',
          type: 'uint8'
        }
      ],
      name: 'setBrightness',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getBrightness',
      outputs: [
        {
          internalType: 'uint8',
          name: '',
          type: 'uint8'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    }
  ];
  private state: BehaviorSubject<boolean>;
  private brightness: BehaviorSubject<number>;

  constructor(web3Service: Web3Service) {
    super(web3Service, BetterLedService.address, BetterLedService.abi);
    this.state = new BehaviorSubject<boolean>(false);
    this.brightness = new BehaviorSubject<number>(100);
    this.getState();
    this.getBrightness();
    this.subscribeEvents();
  }

  public setState(state: boolean) {
    if (state) {
      return this.contract.methods.on().send({from: this.web3Service.getAddress()});
    } else {
      return this.contract.methods.off().send({from: this.web3Service.getAddress()});
    }
  }

  private getState(): void {
    return this.contract.methods.getState().call().then(state => {
      this.state.next(state);
    });
  }

  public async setBrightness(value: number) {
    return await this.contract.methods.setBrightness(value).send({from: this.web3Service.getAddress()});
  }

  private getBrightness(): void {
    return this.contract.methods.getBrightness().call().then(brightness => {
      this.brightness.next(parseInt(brightness, 10));
    });
  }

  private subscribeEvents(): void {
    this.contract.events.ChangeState({}, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        this.state.next(data.returnValues[0]);
      }
    });
    this.contract.events.ChangeBrightness({}, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        this.brightness.next(parseInt(data.returnValues[0], 10));
      }
    });
  }

  public getObservableState() {
    return this.state.asObservable();
  }

  public getObservableBrightness() {
    return this.brightness.asObservable();
  }
}
