import { Component, HostListener, NgZone, OnInit, ViewChild } from '@angular/core';
import { BetterLedService } from './better-led.service';
import { MatSlider } from '@angular/material/slider';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-better-led',
  templateUrl: './better-led.component.html',
  styleUrls: ['./better-led.component.css']
})
export class BetterLedComponent implements OnInit {
  @ViewChild(MatSlider) slider: MatSlider;
  name: string;
  addr: string;
  state: boolean;
  stateObservable;
  brightness: number;
  brightnessObservable;

  constructor(private betterLedService: BetterLedService,
              private ngZone: NgZone) { }

  async ngOnInit() {
    this.name = await this.betterLedService.getName();
    this.addr = this.betterLedService.getAddress();
    this.stateObservable = this.betterLedService.getObservableState();
    this.stateObservable.subscribe(state => {
      this.ngZone.run(() => this.state = state);
    });
    this.brightnessObservable = this.betterLedService.getObservableBrightness();
    this.brightnessObservable.subscribe(brightness => {
      this.ngZone.run(() => this.brightness = brightness);
    });
  }

  setState(event: MatSlideToggleChange) {
    this.betterLedService.setState(event.checked).catch(
      err => event.source.checked = this.state
    );
  }

  setBrightness(value: number): void {
    this.betterLedService.setBrightness(value).catch(
      () => this.slider.value = this.brightness
    );
  }


  @HostListener('window:mouseup', ['$event'])
  mouseUp(event) {
    if (this.slider.value !== this.brightness) {
      this.setBrightness(this.slider.value);
    }
  }
}
