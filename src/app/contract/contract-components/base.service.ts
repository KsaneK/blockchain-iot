import { Inject, Injectable } from '@angular/core';
import { Web3Service } from '../../services/web3.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  protected abi;
  protected address: string;
  protected contract;
  private name: string;

  constructor(protected web3Service: Web3Service, @Inject(String) addr, @Inject(Object) abi) {
    this.address = addr;
    this.contract = web3Service.getContract(abi, addr);
    this.contract.methods.name().call().then(name => {
      this.name = name;
    });
  }

  public async getName() {
    return await this.contract.methods.name().call();
  }

  public getAddress() {
    return this.address;
  }

  public async getComponent() {
    return await this.contract.methods.getComponent().call();
  }
}
