import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuelPumpComponent } from './fuel-pump.component';

describe('FuelPumpComponent', () => {
  let component: FuelPumpComponent;
  let fixture: ComponentFixture<FuelPumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuelPumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuelPumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
