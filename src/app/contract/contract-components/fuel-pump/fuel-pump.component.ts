import { Component, OnInit } from '@angular/core';
import { FuelPumpService } from './fuel-pump.service';
import { Web3Service } from '../../../services/web3.service';

@Component({
  selector: 'app-fuel-pump',
  templateUrl: './fuel-pump.component.html',
  styleUrls: ['./fuel-pump.component.css']
})
export class FuelPumpComponent implements OnInit {
  pump: number;
  calculatedFuel: number;
  ethAmount;
  price;

  constructor(private fuelPumpService: FuelPumpService,
              private web3Service: Web3Service) { }

  async ngOnInit() {
    this.price = this.web3Service.weiToEther(await this.fuelPumpService.getPrice());
    this.calculatedFuel = 0;
  }

  setPump(i: number) {
    console.log('selected pump ' + i);
    this.pump = i;
  }

  calculateFuel(event) {
    this.ethAmount = parseFloat(event.target.value);
    this.calculatedFuel = Math.floor(this.ethAmount / this.price);
    if (isNaN(this.calculatedFuel)) {
      this.calculatedFuel = 0;
    }
  }

  buyFuel() {
    this.fuelPumpService.buyFuel(this.pump, this.web3Service.etherToWei(this.ethAmount.toString()));
  }
}
