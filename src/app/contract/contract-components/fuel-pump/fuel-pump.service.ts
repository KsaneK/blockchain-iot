import { Injectable } from '@angular/core';
import { Web3Service } from '../../../services/web3.service';
import { BaseService } from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class FuelPumpService extends BaseService {
  static address = '0x93b9C46d19E40eb51AaeFAC2bbeb39252b46710b';
  static abi = [
    {
      inputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'constructor'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'uint32',
          name: 'pumpId',
          type: 'uint32'
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'liters',
          type: 'uint256'
        }
      ],
      name: 'BoughtFuel',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'uint256',
          name: 'fuelStock',
          type: 'uint256'
        }
      ],
      name: 'SmallReserves',
      type: 'event'
    },
    {
      constant: true,
      inputs: [],
      name: 'getOwner',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'name',
      outputs: [
        {
          internalType: 'string',
          name: '',
          type: 'string'
        }
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getComponent',
      outputs: [
        {
          internalType: 'bytes32[]',
          name: '',
          type: 'bytes32[]'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'bytes32[]',
          name: 'zipFile',
          type: 'bytes32[]'
        }
      ],
      name: 'setComponent',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getStock',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getPrice',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'uint32',
          name: 'stock',
          type: 'uint32'
        }
      ],
      name: 'addToStock',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'uint32',
          name: 'threshold',
          type: 'uint32'
        }
      ],
      name: 'setNotifyThreshold',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'uint256',
          name: 'weiPrice',
          type: 'uint256'
        }
      ],
      name: 'changePrice',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'uint32',
          name: 'pumpId',
          type: 'uint32'
        }
      ],
      name: 'buyFuel',
      outputs: [],
      payable: true,
      stateMutability: 'payable',
      type: 'function'
    }
  ];

  constructor(web3Service: Web3Service) {
    super(web3Service, FuelPumpService.address, FuelPumpService.abi);
  }

  public async getPrice() {
    return await this.contract.methods.getPrice().call();
  }

  public buyFuel(pump: number, v: string) {
    this.contract.methods.buyFuel(pump).send({from: this.web3Service.getAddress(), value: v});
  }
}
