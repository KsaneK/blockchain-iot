import { TestBed } from '@angular/core/testing';

import { FuelPumpService } from './fuel-pump.service';

describe('FuelPumpServiceService', () => {
  let service: FuelPumpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FuelPumpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
