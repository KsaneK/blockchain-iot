import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseService } from '../base.service';
import { Web3Service } from '../../../services/web3.service';

@Component({
  selector: 'app-not-implemented',
  templateUrl: './not-implemented.component.html',
  styleUrls: ['./not-implemented.component.css']
})
export class NotImplementedComponent implements OnInit {
  static abi = [
    {
      constant: true,
      inputs: [],
      name: 'getComponent',
      outputs: [
        {
          internalType: 'bytes32[]',
          name: '',
          type: 'bytes32[]'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'bytes32[]',
          name: 'zipFile',
          type: 'bytes32[]'
        }
      ],
      name: 'setComponent',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'name',
      outputs: [
        {
          internalType: 'string',
          name: '',
          type: 'string'
        }
      ],
      payable: false,
      stateMutability: 'pure',
      type: 'function'
    }
  ];
  addr: string;
  private service: BaseService;

  constructor(private route: ActivatedRoute,
              private web3Service: Web3Service) {
  }

  ngOnInit(): void {
    this.addr = this.route.snapshot.paramMap.get('addr');
    this.service = new BaseService(this.web3Service, this.addr, NotImplementedComponent.abi);
  }

  hexStringToByte(str): Uint8Array {
    if (!str) {
      return new Uint8Array();
    }

    const byteArr = [];
    for (let i = 2, len = str.length; i < len; i += 2) {
      byteArr.push(parseInt(str.substr(i, 2), 16));
    }

    return new Uint8Array(byteArr);
  }

  async download() {
    const downloadFile = (blob, fileName) => {
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = fileName;
      document.body.append(link);
      link.click();
      link.remove();
      window.addEventListener('focus', e => URL.revokeObjectURL(link.href), {once: true});
    };


    const name = await this.service.getName();
    const component = await this.service.getComponent();
    const bytearray = [];
    for (const bytes32 of component) {
      for (const byte of this.hexStringToByte(bytes32)) {
        bytearray.push(byte);
      }
    }
    const blob = new Blob([new Uint8Array(bytearray)], {type: 'application/zip'});
    downloadFile(blob, name);
  }
}
