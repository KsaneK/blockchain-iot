import { LedComponent } from './contract-components/led/led.component';
import { BetterLedComponent } from './contract-components/better-led/better-led.component';
import { FuelPumpComponent } from './contract-components/fuel-pump/fuel-pump.component';

export class Contracts {

  // Define your contracts here
  private static addressToRouteMap: Map<string, any> = new Map([
    ['0xc2331A067871C7939beF877E9541669AdB8e9891', {route: 'led', component: LedComponent}],
    ['0xDfcAF6F7B74c38928B36C8d0F2141e29B169B63c', {route: 'better-led', component: BetterLedComponent}],
    ['0x93b9C46d19E40eb51AaeFAC2bbeb39252b46710b', {route: 'fuel-pump', component: FuelPumpComponent}],
  ]);

  public static getRoute(addr: string) {
    if (Contracts.addressToRouteMap.has(addr)) {
      return Contracts.addressToRouteMap.get(addr).route;
    }
    return addr;
  }

  public static getRoutes() {
    return this.addressToRouteMap.values();
  }

  private static invert(obj) {
    const map = new Map();
    for (const [key, value] of obj) {
      map.set(value.route, key);
    }
    return map;
  }
}
