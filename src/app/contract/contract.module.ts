import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LedComponent } from './contract-components/led/led.component';
import { NotImplementedComponent } from './contract-components/not-implemented/not-implemented.component';
import { MaterialModule } from '../material.module';
import { RouterModule, Routes } from '@angular/router';
import { Contracts } from './contracts';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BetterLedComponent } from './contract-components/better-led/better-led.component';
import { FuelPumpComponent } from './contract-components/fuel-pump/fuel-pump.component';


const routes: Routes = [];

for (const route of Contracts.getRoutes()) {
  routes.push({path: 'contract/' + route.route, component: route.component});
}
routes.push({path: 'contract/:addr', component: NotImplementedComponent});

@NgModule({
  declarations: [
    LedComponent,
    NotImplementedComponent,
    BetterLedComponent,
    FuelPumpComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FontAwesomeModule,
    RouterModule.forRoot(routes),
  ]
})
export class ContractModule { }
