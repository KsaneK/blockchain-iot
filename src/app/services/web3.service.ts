import { Injectable } from '@angular/core';
import Web3 from 'web3';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { AccountInfo } from '../interfaces/account-info';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';

declare let window: any;

@Injectable({
  providedIn: 'root'
})
export class Web3Service {

  accountChangeSubject: Subject<AccountInfo>;
  connectedSubject: BehaviorSubject<boolean>;
  enable: Promise<any>;
  httpProvider: any;
  web3: Web3;

  constructor() {
    this.accountChangeSubject = new Subject<AccountInfo>();

    if (window.ethereum !== undefined) {
      this.web3 = new Web3(window.ethereum);
      this.setProvider();
      this.enable = this.enableMetaMaskAccount();
      this.connectedSubject = new BehaviorSubject<boolean>(this.isConnected());

      window.ethereum.on('accountsChanged', accounts => this.pushAccountInfo(accounts[0]));
    } else {
      this.connectedSubject = new BehaviorSubject<boolean>(false);
    }
  }

  private setProvider() {
    if (typeof this.web3 !== 'undefined') {
      this.httpProvider = this.web3.currentProvider;
    } else {
      this.httpProvider = new Web3.providers.HttpProvider(environment.httpProviderUri);
    }
  }

  public getObservableAccountInfo(): Observable<AccountInfo> {
    return this.accountChangeSubject.asObservable();
  }

  public getObservableConnectionStatus(): Observable<boolean> {
    return this.connectedSubject.asObservable();
  }

  private async pushAccountInfo(addr: string) {
    if (isNotNullOrUndefined(addr)) {
      this.connectedSubject.next(true);
      this.accountChangeSubject.next(await this.getAccountInfo(addr));
    } else {
      this.connectedSubject.next(false);
    }
  }

  public async getAccountInfo(addr: string) {
    return {
      address: addr,
      balance: this.weiToEther(await this.getBalance(addr))
    } as AccountInfo;
  }

  private async enableMetaMaskAccount(): Promise<any> {
    let enable = false;
    await new Promise((resolve, reject) => {
      enable = window.ethereum.enable();
    });
    return Promise.resolve(enable);
  }

  public isConnected(): boolean {
    if (this.httpProvider == null) {
      return false;
    }
    return this.httpProvider.isConnected() &&
      isNotNullOrUndefined(this.web3.eth.accounts.givenProvider.selectedAddress);
  }

  public getAddress(): string {
    return this.web3.eth.accounts.givenProvider.selectedAddress;
  }

  public weiToEther(value: string) {
    return this.web3.utils.fromWei(value, 'ether');
  }

  public etherToWei(value: string) {
    return this.web3.utils.toWei(value, 'ether');
  }

  public getBalance(address: string) {
    return this.web3.eth.getBalance(address);
  }

  public getContract(abi, addr) {
    return new this.web3.eth.Contract(abi, addr);
  }

  public isAddressValid(addr: string) {
    try {
      this.web3.utils.toChecksumAddress(addr);
      return true;
    } catch {
      return false;
    }
  }
}
