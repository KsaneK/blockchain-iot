import { Injectable, OnDestroy } from '@angular/core';
import { Web3Service } from './web3.service';
import { Contract } from '../interfaces/contract';
import { BehaviorSubject, SubscriptionLike } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContractStorageService implements OnDestroy {
  private address = environment.iotContractStorageAddress;
  private abi = [
    {
      inputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'constructor'
    },
    {
      anonymous: false,
      inputs: [
        {
          components: [
            {
              internalType: 'string',
              name: 'name',
              type: 'string'
            },
            {
              internalType: 'address',
              name: 'addr',
              type: 'address'
            },
            {
              internalType: 'bool',
              name: 'is_active',
              type: 'bool'
            }
          ],
          indexed: false,
          internalType: 'struct IotContractStorage.IotContract',
          name: 'iotContract',
          type: 'tuple'
        }
      ],
      name: 'ContractActivated',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          components: [
            {
              internalType: 'string',
              name: 'name',
              type: 'string'
            },
            {
              internalType: 'address',
              name: 'addr',
              type: 'address'
            },
            {
              internalType: 'bool',
              name: 'is_active',
              type: 'bool'
            }
          ],
          indexed: false,
          internalType: 'struct IotContractStorage.IotContract',
          name: 'iotContract',
          type: 'tuple'
        }
      ],
      name: 'ContractAdded',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          components: [
            {
              internalType: 'string',
              name: 'name',
              type: 'string'
            },
            {
              internalType: 'address',
              name: 'addr',
              type: 'address'
            },
            {
              internalType: 'bool',
              name: 'is_active',
              type: 'bool'
            }
          ],
          indexed: false,
          internalType: 'struct IotContractStorage.IotContract',
          name: 'iotContract',
          type: 'tuple'
        }
      ],
      name: 'ContractDeactivated',
      type: 'event'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'newOwner',
          type: 'address'
        }
      ],
      name: 'changeOwner',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getOwner',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'addr',
          type: 'address'
        }
      ],
      name: 'addContract',
      outputs: [
        {
          internalType: 'string',
          name: '',
          type: 'string'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'addr',
          type: 'address'
        }
      ],
      name: 'deactivateContract',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'addr',
          type: 'address'
        }
      ],
      name: 'activateContract',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getContractCount',
      outputs: [
        {
          internalType: 'uint32',
          name: '',
          type: 'uint32'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'getContracts',
      outputs: [
        {
          components: [
            {
              internalType: 'string',
              name: 'name',
              type: 'string'
            },
            {
              internalType: 'address',
              name: 'addr',
              type: 'address'
            },
            {
              internalType: 'bool',
              name: 'is_active',
              type: 'bool'
            }
          ],
          internalType: 'struct IotContractStorage.IotContract[]',
          name: '',
          type: 'tuple[]'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: true,
      inputs: [
        {
          internalType: 'address',
          name: 'addr',
          type: 'address'
        }
      ],
      name: 'doesExist',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'addr',
          type: 'address'
        }
      ],
      name: 'addToFavourite',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          internalType: 'address',
          name: 'addr',
          type: 'address'
        }
      ],
      name: 'removeFromFavourite',
      outputs: [],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [
        {
          internalType: 'address',
          name: 'addr',
          type: 'address'
        }
      ],
      name: 'isFavourite',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    }
  ];
  private contract;
  private iotContractsSubject: BehaviorSubject<Contract[]>;
  private web3Service: Web3Service;
  private contractAddedSub: SubscriptionLike;

  constructor(web3Service: Web3Service) {
    this.web3Service = web3Service;
    this.iotContractsSubject = new BehaviorSubject<Contract[]>([]);
    this.contract = this.web3Service.getContract(this.abi, this.address);

    this.getContracts();
    this.subscribeEvents();
  }

  private getContracts() {
    const iotContracts: Contract[] = [];
    this.contract.methods.getContracts().call().then(contracts => {
      contracts.forEach(c => iotContracts.push({name: c.name, address: c.addr} as Contract));
      this.iotContractsSubject.next(iotContracts);
    });
  }

  private subscribeEvents() {
    this.contractAddedSub = this.contract.events.ContractAdded({}, (err, event) => {
      const eventContract = event.returnValues.iotContract;
      const newContract = {name: eventContract.name, address: eventContract.addr} as Contract;
      const contracts = this.iotContractsSubject.value;
      contracts.push(newContract);
      this.iotContractsSubject.next(contracts);
    });
  }

  public getObservableContracts() {
    return this.iotContractsSubject.asObservable();
  }

  public addContract(addr: string) {
    try {
      return this.contract.methods.addContract(addr).send({from: this.web3Service.getAddress()});
    } catch (e) {
      if (e.arg === 'addr') {
        return {error: 'addr', details: 'Invalid address'};
      }
      return {error: 'unexpected', details: 'Unexpected error'};
    }
  }

  public async doesExist(addr: string) {
    try {
      return await this.contract.methods.doesExist(addr).call();
    } catch (e) {
      if (e.arg === 'addr') {
        return false;
      }
    }
  }

  public async isOwner() {
    return await this.contract.methods.isOwner().call();
  }

  ngOnDestroy(): void {
    this.contractAddedSub.unsubscribe();
  }

  public async isFavourite(addr: string) {
    return await this.contract.methods.isFavourite(addr).call();
  }

  public async addFavourite(addr: string) {
    return await this.contract.methods.addToFavourite(addr).send({from: this.web3Service.getAddress()});
  }

  public async removeFavourite(addr: string) {
    return await this.contract.methods.removeFromFavourite(addr).send({from: this.web3Service.getAddress()});
  }
}
