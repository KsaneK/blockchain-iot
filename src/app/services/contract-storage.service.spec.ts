import { TestBed } from '@angular/core/testing';

import { ContractStorageService } from './contract-storage.service';

describe('ContractStorageService', () => {
  let service: ContractStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContractStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
