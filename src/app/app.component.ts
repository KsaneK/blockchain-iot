import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { Web3Service } from './services/web3.service';
import { AccountInfo } from './interfaces/account-info';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'SmartContracts';
  accountInfo: AccountInfo;
  connectionStatus: boolean;
  subscribeConnectionStatus: Subscription;
  subscribeAccountInfo: Subscription;
  mobile: boolean;

  constructor(private web3Service: Web3Service,
              private ngZone: NgZone) {}

  public ngOnInit() {
    const addr = this.web3Service.getAddress();
    if (addr) {
      this.web3Service.getAccountInfo(addr).then(v => this.accountInfo = v);
    }
    this.subscribeAccountInfo = this.web3Service.getObservableAccountInfo()
      .subscribe(value => {
        this.ngZone.run(() => this.accountInfo = value);
      });
    this.subscribeConnectionStatus = this.web3Service.getObservableConnectionStatus()
      .subscribe(value => {
        this.ngZone.run(() => this.connectionStatus = value);
      });
    if (window.screen.width <= 720) { // 768px portrait
      this.mobile = true;
    }
  }

  public ngOnDestroy() {
    this.subscribeAccountInfo.unsubscribe();
    this.subscribeConnectionStatus.unsubscribe();
  }
}
