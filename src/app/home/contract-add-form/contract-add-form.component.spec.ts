import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractAddFormComponent } from './contract-add-form.component';

describe('ContractAddFormComponent', () => {
  let component: ContractAddFormComponent;
  let fixture: ComponentFixture<ContractAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractAddFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
