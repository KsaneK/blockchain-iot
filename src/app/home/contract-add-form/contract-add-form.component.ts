import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../../services/web3.service';
import { AbstractControl, FormControl } from '@angular/forms';
import { ContractStorageService } from '../../services/contract-storage.service';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { MatSnackBar } from '@angular/material/snack-bar';


class AddressValidator {
  static addressValid(web3Service: Web3Service) {
    return (control: AbstractControl) => {
      if (web3Service.isAddressValid(control.value)) {
        return null;
      } else {
        return {'bad-address': ''};
      }
    };
  }
}

@Component({
  selector: 'app-contract-add-form',
  templateUrl: './contract-add-form.component.html',
  styleUrls: ['./contract-add-form.component.css']
})
export class ContractAddFormComponent implements OnInit {
  private contractStorageService: ContractStorageService;
  private web3Service: Web3Service;
  addressFormControl: FormControl;
  errorMessage: string;
  private snackBar: MatSnackBar;

  constructor(web3Service: Web3Service,
              contractStorageService: ContractStorageService,
              snackBar: MatSnackBar) {
    this.snackBar = snackBar;
    this.web3Service = web3Service;
    this.contractStorageService = contractStorageService;
  }

  ngOnInit(): void {
    this.addressFormControl = new FormControl('', [AddressValidator.addressValid(this.web3Service)]);
  }

  public addContract(addr: string) {
    this.errorMessage = '';
    this.contractStorageService.doesExist(addr).then(doesExist => {
      if (doesExist) {
        this.errorMessage = 'Contract with this address already exists in storage';
      } else {
        const ret = this.contractStorageService.addContract(addr);
        if (typeof ret.then === 'function') {
          ret
            .then(() => {
              this.snackBar.open('Contract added!', 'Ok!');
            })
            .catch(err => {
              if (err.code === 4001) {
                this.errorMessage = 'Transaction was denied';
              } else if (err.code === -32603) {
                this.errorMessage = 'Error while sending transaction. Are you sure your contract implements name() function?';
              }
            });
        } else if (isNotNullOrUndefined(ret)) {
          this.errorMessage = ret.details;
        }
      }
    });
  }
}
