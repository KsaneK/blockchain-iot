import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Contract } from '../../interfaces/contract';
import { ContractStorageService } from '../../services/contract-storage.service';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { Contracts } from '../../contract/contracts';
import { MatSort, Sort } from '@angular/material/sort';

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.css']
})
export class ContractListComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  displayedColumns: string[] = ['favourite', 'name', 'address'];
  private contractStorageService: ContractStorageService;
  contracts: MatTableDataSource<Contract>;
  private contractsSub: Subscription;

  constructor(contractStorageService: ContractStorageService) {
    this.contractStorageService = contractStorageService;
  }

  async ngOnInit() {
    this.contractsSub = this.contractStorageService.getObservableContracts()
      .subscribe(async (contracts) => {
        for (const contract of contracts) {
          contract.isFavourite = await this.contractStorageService.isFavourite(contract.address);
        }
        this.contracts = new MatTableDataSource<Contract>(contracts);
        this.contracts.sort = this.sort;
      });
  }

  ngOnDestroy(): void {
    this.contractsSub.unsubscribe();
  }

  public getRoute(addr: string) {
    return Contracts.getRoute(addr);
  }

  public async isFavourite(addr: string) {
    this.contractStorageService.isFavourite(addr).then((data) => {
      console.log(data);
    });
  }

  async addFavourite(contract: Contract) {
    this.contractStorageService.addFavourite(contract.address).then(() => {
      contract.isFavourite = true;
    }).catch((err) => {
      console.log(err);
    });
  }

  async removeFavourite(contract: Contract) {
    this.contractStorageService.removeFavourite(contract.address).then(() => {
      contract.isFavourite = false;
    }).catch((err) => {
      console.log(err);
    });
  }

  applyFilter(event: Event) {
    if (this.contracts.filter === 'true') {
      this.contracts.filter = '';
    } else {
      this.contracts.filter = 'true';
    }
  }
}
