export interface AccountInfo {
  address: string;
  balance: string;
}
