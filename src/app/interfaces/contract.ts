export interface Contract {
  name: string;
  address: string;
  isFavourite: boolean;
}
