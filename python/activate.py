import argparse
import web3
import json
from eth_typing import URI

with open("../build/contracts/IotContractStorage.json", "r") as f:
    iot_contract_storage_json = json.load(f)

iot_contract_storage_addr = iot_contract_storage_json.get("networks").get("5777").get("address")

parser = argparse.ArgumentParser()
parser.add_argument("address", help="contract address which should be activated")
args = parser.parse_args()


abi = [
    {
        "constant": False,
        "inputs": [
            {
                "internalType": "address",
                "name": "addr",
                "type": "address"
            }
        ],
        "name": "activateContract",
        "outputs": [],
        "payable": False,
        "stateMutability": "nonpayable",
        "type": "function"
    }
]

provider = web3.providers.HTTPProvider(URI('http://localhost:7545'))
w3 = web3.Web3(provider)
w3.eth.defaultAccount = w3.eth.accounts[0]
addr = w3.toChecksumAddress(iot_contract_storage_addr)
iot_contract_storage = w3.eth.contract(address=addr, abi=abi)
tx_receipt = iot_contract_storage.functions.activateContract(args.address).transact()
w3.eth.waitForTransactionReceipt(tx_receipt)
print(f"Transaction receipt: {tx_receipt}")
