import asyncio

import web3
from eth_typing import URI

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)


def set_led_state(state):
    # TODO - handle setting led state
    if state:
        p.ChangeDutyCycle(100 - led_contract.functions.getBrightness().call())
    else:
        p.ChangeDutyCycle(0)


def set_led_brightness(value):
    if led_contract.functions.getState().call() == GPIO.HIGH:
        p.ChangeDutyCycle(100 - value)


async def handle_change_state(ev_filter, poll_interval):
    while True:
        for ev in ev_filter.get_new_entries():
            state = ev.get('args').get('_value')
            print(f"Led state: {state}")
            set_led_state(state)
        await asyncio.sleep(poll_interval)


async def handle_change_brightness(ev_filter, poll_interval):
    while True:
        for ev in ev_filter.get_new_entries():
            new_value = ev.get('args').get('value')
            print(f"New brightness: {new_value}")
            set_led_brightness(new_value)
        await asyncio.sleep(poll_interval)


led_abi = [
    {
        "anonymous": False,
        "inputs": [
            {
                "indexed": False,
                "internalType": "bool",
                "name": "_value",
                "type": "bool"
            }
        ],
        "name": "ChangeState",
        "type": "event"
    },
    {
        "constant": True,
        "inputs": [],
        "name": "getState",
        "outputs": [
            {
                "internalType": "bool",
                "name": "",
                "type": "bool"
            }
        ],
        "payable": False,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": True,
        "inputs": [],
        "name": "getBrightness",
        "outputs": [
            {
                "internalType": "uint8",
                "name": "",
                "type": "uint8"
            }
        ],
        "payable": False,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "anonymous": False,
        "inputs": [
            {
                "indexed": False,
                "internalType": "uint8",
                "name": "value",
                "type": "uint8"
            }
        ],
        "name": "ChangeBrightness",
        "type": "event"
    },
]

print("Connecting to blockchain")
provider = web3.providers.HTTPProvider(URI('http://localhost:7545'))
print(f"connected?: {provider.isConnected()}\n")
w3 = web3.Web3(provider)
w3.eth.defaultAccount = w3.eth.accounts[0]

led_contract_addr = w3.toChecksumAddress("0xDfcAF6F7B74c38928B36C8d0F2141e29B169B63c")
led_contract = w3.eth.contract(address=led_contract_addr, abi=led_abi)

power = led_contract.functions.getState().call()
brightness = led_contract.functions.getBrightness().call()
print(f"Led state: {power}")
print(f"Brightness: {brightness}")

p = GPIO.PWM(18, 200)
p.start(100 - brightness)

change_state_filter = led_contract.events.ChangeState.createFilter(fromBlock='latest')
change_brightness_filter = led_contract.events.ChangeBrightness.createFilter(fromBlock='latest')

loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(
        asyncio.gather(
            handle_change_state(change_state_filter, 2),
            handle_change_brightness(change_brightness_filter, 2),
        )
    )
finally:
    loop.close()
