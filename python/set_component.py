import argparse
import web3
from eth_typing import URI

parser = argparse.ArgumentParser()
parser.add_argument("address", help="contract address to which the component should be sent")
parser.add_argument("file", help="directory of zip file with angular component")
args = parser.parse_args()


def chunks(iterable, chunk_size):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(iterable), chunk_size):
        yield iterable[i:i + chunk_size]


with open(args.file, "rb") as f:
    zip_file_as_bytes32 = [chunk for chunk in chunks(f.read(), 32)]


abi = [
    {
        "constant": False,
        "inputs": [
            {
                "internalType": "bytes32[]",
                "name": "zipFile",
                "type": "bytes32[]"
            }
        ],
        "name": "setComponent",
        "outputs": [],
        "payable": False,
        "stateMutability": "nonpayable",
        "type": "function"
    }
]

provider = web3.providers.HTTPProvider(URI('http://localhost:7545'))
w3 = web3.Web3(provider)
w3.eth.defaultAccount = w3.eth.accounts[0]
addr = w3.toChecksumAddress(args.address)
led_contract = w3.eth.contract(address=addr, abi=abi)
tx_receipt = led_contract.functions.setComponent(zip_file_as_bytes32).transact()
w3.eth.waitForTransactionReceipt(tx_receipt)
print(f"Transaction receipt: {tx_receipt}")
